/**
* @summary Manejamos todo los helpers de string aca adentro...
*
* @description -  ...
*
* @author Aditivo Interactive Group <info@aditivointeractivegroup.com> / Colo Baggins
*
* @since  1.0.0
*
* @see {@link http://www.aditivointeractivegroup.com}  
*
* @todo Complete documentation...
*/

var StringHelpers = function() {

	return {

		removeWhiteSpace: function(text){

			return text.replace(/\s/g, '');

		},

		copyToClipboard: function(text){

			var textArea = document.createElement("textarea");

			textArea.style.position = 'fixed';

			textArea.style.top = 0;

			textArea.style.left = 0;

			textArea.style.width = '2em';

			textArea.style.height = '2em';

			textArea.style.padding = 0;

			textArea.style.border = 'none';

			textArea.style.outline = 'none';

			textArea.style.boxShadow = 'none';

			textArea.style.background = 'transparent';

			textArea.value = text;//$(this).text();

			document.body.appendChild(textArea);

			textArea.focus();

			textArea.select();

			try {

				var successful = document.execCommand('copy');

				var msg = successful ? 'successful' : 'unsuccessful';

				console.log('Copying text command was ' + msg);

			} catch (err) {

				console.log('Oops, unable to copy');

			}

			console.log(textArea.value);

			document.body.removeChild(textArea);

		}

	}

}