/**
* @summary Manejamos todo los helpers de color aca adentro...
*
* @description -  ...
*
* @author Aditivo Interactive Group <info@aditivointeractivegroup.com> / Colo Baggins
*
* @since  1.0.0
*
* @see {@link http://www.aditivointeractivegroup.com}
*
* @todo Complete documentation...
*/


var ColorsHelpers = function() {

	return {

		/*
		* * * divs con background - color * * *
		*
		*	Recibe color [ rgb(0,0,0) ] , devuelve color hexadecimal [ #FFFFFF ]
		*	! Revisar funcionamiento con rgba()
		*
		* @param - colorval - color formato rgb()
		* @return - color formato hex
		*/

		hexc : function(colorval) {

			var parts = colorval.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);

			delete(parts[0]);

			for (var i = 1; i <= 3; ++i) {

				parts[i] = parseInt(parts[i]).toString(16);

				if (parts[i].length == 1) parts[i] = '0' + parts[i];

			}

			hexColor = '#' + parts.join('');

			return hexColor;

		},

		colorFromImg : function(imgElement){



		}


    }

}
