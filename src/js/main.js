/**
* @summary App for Museo Del Holocausto / Buenos Aires / Argentina.
*
* @description - then i will add this.
*
* @author Aditivo Interactive Group S.A <info@aditivointeractivegroup.com>
*
* @since  1.0.0
*
* @see {@link http://www.aditivointeractivegroup.com}
*
* @todo Complete documentation.
*/

//"use strict";

/**
* @function Project
* @description Initializa la funcion Project con todos sus metodos dentro.
*/

var Project = function() {

	//usemos todas las variables globales al comienzo del code....

	var hexColor = '';

	var overlayElmt = $(".overlay");

	var containerCloseBtn = $(".container-btn-close-overlay");

	//*** Video

	var videoOverlayElmt = $('.video-overlay');	//Video overlay

	var videoCont = $('.video-container');	//Video container

	var videoElmt = $('#naziVideo').get(0);			//Video element

	var videoControls = $("#video-controls");

	var videoPlayBtn = $("#play-pause");							//Play - puse btn
	var playIcon = $("#playBtn");
	var pauseIcon = $("#pauseBtn");


	//Video, mostrar play por defecto, pause oculto  btn
	playIcon.hide();
	pauseIcon.show();

	var videoSeekBarBtn = $("#seek-bar");

	var videoVolumeBarBtn = $("#volume-bar");

	var videoVolumeMuteBtn = $("#mute");
	//Video, mostrar volume icon correspondiente btn
	var volumeOff = $("#volumeOff");
	var volumeOn = $("#volumeOn");

	volumeOff.hide();
	volumeOn.show();

	// *** Video ends


	var overlayTitle = $('.overlay').find('.description-container-box h3');
	var overlayDescription = $('.overlay').find('.description-container-box .desc');

	var overlaySlider = $('.slider-container');


	//Scrollbar element

	var scrollElemt = document.querySelector("#my-scrollbar");
	var scrollTrackElmt = $("#my-scrollbar .scrollbar-track");

	//mismo, si inifializamos alguna clase, la mandamos aca arriba

	var colorHelpers = new ColorsHelpers();

	//este no lo vamos a necesitar, pero tal vez ayuda para entneder mejor como
	//podemos organizar los helpers...

	var mobileHelpers = new MobileHelpers();

	var isRetina = mobileHelpers.isRetina();

	var isMobile = mobileHelpers.isMobile();

	console.log("is retina: " + isRetina + ". is mobile: " + isMobile);

	//aca dejo un audio para testeo...

	var audio = new Audio('audio/close.mp3');


	/**
	* @function initEnterAnimate
	* @description Animacion de entrada en botones de la app (fade with ease)
	*
	*/

	var initEnterAnimate = function(){

		console.log("Fade in btns");

		var delayElmt = 0;

		/*
		$('.btn-act').each(function(i, obj){
		  delayElmt =+ delayElmt + (i/80);
			console.log('btns class: btn-'+i+' - delayElmt: '+delayElmt);
			TweenMax.to($('.btn-'+(i+1)), .2, {opacity:'1', ease: Sine.easeOut, delay:delayElmt})
		});
		*/

	}

	/**
	* @function addListeners
	* @description Por una cuestion de orden y prolijidad, usemos siempre nombres d variables
	* y funciones en ingles, para evitar cualquier bardo.
	* Generalmente creo una funcion addListeners donde mando todos los eventos de click, touch, timers, etc.
	* Es una suerte de inicializador.
	*
	* No es necesario comentar todas las funciones, lo ideal es que sus nombres las hagan autoexplicativas,
	* pero muchas veces esta bueno tener una explicacion de que vas a encontrar dentro del metodo.
	*/

	var addListeners = function(){


		$('.btn-act').click(function(){

			var btnId = this.id;

			console.log("id btn: "+btnId);

			//Cargar imagenes en slider segun boton clikeado
			//searchSliderImgs(btnId);

			//si el modulo es el 1, cargamos el video :)

			if(btnId == 0){

				loadVideo();

				return;

			}


			var btnColor = $("#" + btnId).css("backgroundColor");

			//si tenemos encapsulada hexc mandamos a hacer los calculos a otro lado...

			//hexColor = colorHelpers.hexc(btnColor);

			//y luego se lo podemos pasar a openOverlay...

			//Cargar data de boton/seccion
			jsonData(btnId);

			//openOverlay(hexColor);
			openOverlay('#84181A');

			//y si te queres hacer el cheto y pro, podes resumir un mas:

			//openOverlay(colorHelpers.hexc(btnColor));


		});


		//**
		//Cerrar overlay mediante closeBtn
		//**
		containerCloseBtn.click(function(){

			//Get attribute - Saber si cerramos video o sliders
			var dataSectionAttr = $(this).data('section');
			console.log("close btn, data: "+dataSectionAttr);


			//Animacion cierre closeBtn
			TweenMax.to($(this), 0.8, {opacity:0, ease: Power3.easeOut});

			//Resto de animacion de contenido y funciones
			closeOverlay(dataSectionAttr);
		});


		//**
		//Cerrar video overlay - Listener para cerrar videoOverlay al clickear en cualquier parte del mismo
		//** Diferencia el evento click entre element contenedor e hijo
		//**

		$('#naziVideo').on('click', function(e) {
		    e.stopPropagation();
		});

		$('#video-controls').on('click', function(e) {
		    e.stopPropagation();
		});

		videoOverlayElmt.on('click', function(e){
				console.log("Video overlay clicked");
				closeOverlay("video");

		});




		//**
		// Play / Pause video
		//**
		videoPlayBtn.click(function(){
				if(videoElmt.paused==true){
					console.log("Play video");
					videoElmt.play();

					$("#playBtn").hide();
					$("#pauseBtn").show();

				}else{
					console.log("pausar video");
					videoElmt.pause();

					$("#pauseBtn").hide();
					$("#playBtn").show();
				}

		});


		//**
		//Seek bar video - Cambiar al mover slider track
		//**
		videoSeekBarBtn.change(function(){
			//Calcular nuevo time
			var time = videoElmt.duration * ($(this).val() / 100);

			//Update the video time
			videoElmt.currentTime = time;

		});


		//**
		// Update the seek bar as the video plays
		//**
		$("#naziVideo").on('timeupdate', function(){

				// Calculate the slider value
			  var sliderVal = (100 / this.duration) * this.currentTime;

			  // Update the slider value
			  videoSeekBarBtn.val(sliderVal);
		});

		//**
		//Parar video al buscar(mantener apretado) en la seekbar
		//**
		videoSeekBarBtn.mousedown(function(){
			videoElmt.pause();
		});

		//**
		// Reanudar video al soltar la seekbar
		//**
		videoSeekBarBtn.mouseup(function(){
			videoElmt.play();
		});

		//**
		//Video Volume
		//**
		videoVolumeBarBtn.change(function(){
			//Update the video volume
			videoElmt.volume = $(this).val();
			console.log("volume: "+videoElmt.volume);
		});

		//**
		//Mute volume
		//**
		videoVolumeMuteBtn.click(function(){

			if($("#naziVideo").prop("muted")){
				videoElmt.muted = false;
				$("#volumeOff").hide();
				$("#volumeOn").show();

				videoVolumeBarBtn.val(1);
			}else{
				videoElmt.muted = true;
				$("#volumeOn").hide();
				$("#volumeOff").show();

				videoVolumeBarBtn.val(0);
			}
			console.log("muted? "+ $("#naziVideo").prop("muted"));

		});

	}
	//End addListener



	/**
	* @function loadVideo
	* @description Carga de video... Abre overlay
	*/

	var loadVideo = function(){

		console.log("Cargando video");

		//Play video al abrir overlay
		videoElmt.play();
		videoElmt.muted = false; 
		//videoElmt.muted = true; //Silencio por defecto !!!!!

		//Abrir videoOverlay
		TweenMax.to(videoOverlayElmt, 1.3, {display:'block', backgroundColor:'rgba(0,0,0,.5)', ease: Power3.easeOut});

		//Mostrar video
		TweenMax.to($(".video-container"), .8, {opacity:'1', ease: Power3.easeOut, delay: .4});

		//Mostrar video-controls
		//TweenMax.to(videoControls, .5, {opacity:'1', ease: Power3.easeOut, delay:.8});

		showCloseBtn();
	}



	/**
	* @function openOverlay
	* @description Abre el overlay
	*
	* @param - hexColor - Identifica el color a utilizar.
	*/

	var openOverlay = function(hexColor){

		overlayElmt.css("backgroundColor", hexColor);

		//Mostrar overlay
		$(overlayElmt).css('opacity', '1');
		TweenMax.to(overlayElmt, .8, {display:'block', height:'100%', onComplete:completeOpenHandler, ease: Sine.easeOut});

		//Scroll Elmt animation
		TweenMax.to($(".scroll-content"), 1, {opacity:'1', onComplete:showShadowScrollDesc, ease: Power3.easeOut, delay:.9});
		//Scroll Track Y
		TweenMax.to($("#my-scrollbar .scrollbar-track"), .2, {opacity:'1', ease: Power3.easeOut, delay:1.4});

		//Mostrar el contenido dentro del overlay
		TweenMax.to(overlayTitle, .6, {opacity:'1', ease: Power3.easeOut, delay:.3});
		TweenMax.to(overlayDescription, .6, {opacity:'1', ease: Power3.easeOut, delay: .8});

		//Mostrar slider
		TweenMax.to(overlaySlider, .4, {opacity:'1', ease: Power3.easeOut, delay: .7});

		showCloseBtn();
	}

	/**
	* @function showShadowScrollDesc
	* @description - MUestra sombra debajo de scroll - Se ejecuta cuando se termina de mostrar la descripcion dentro del scroll
	*/
	var showShadowScrollDesc = function(){
		TweenMax.to(	$(".bottom-shadow"), .2, {opacity:'1', ease: Power3.easeOut});
		console.log("show shadow description scroll");
	}

	/**
	* @function hideShadowScrollDesc
	* @description - Oculta sombra debajo de scroll - Se ejecuta cuando se termina de ocultar la descripcion del scroll
	*/
	var hideShadowScrollDesc = function(){
		TweenMax.to(	$(".bottom-shadow"), .2, {opacity:'0', ease: Power3.easeOut});
		console.log("hide shadow description scroll");
	}

	/**
	* @function completeCloseHandler
	* @description Se ejecuta cuando se termino de cerrar el overlay.
	*/

	var completeCloseHandler = function(){

		//Quitar Overlay
		overlayElmt.css('display', 'none');
		overlayElmt.css('height', '0px');

		//Quitar Video overlay
		videoOverlayElmt.css('display', 'none');

		//Remover imgs de slider
		removeSliderImgs();
		console.log("overlay closed");
	}

	/**
	* @function completeOpenHandler
	* @description ...
	*/

	var completeOpenHandler = function(){

		console.log("overlay opened complete");

		//Indispensable para que el slider se muestre y calcule las dimensiones de su contenedor, cuando el overlay esta 100% abierto
		$('.single-img-slider').slick('refresh');
	}

	/**
	* @function closeOverlay
	* @description Cierra overlay y contenido (vide y galeria)
	*
	* @param section - Seccion a cerrar (video btn1 o sliders, resto de botones)
	*/

	var closeOverlay = function(section){

		if(section=="slider"){
			//Animar slider
			TweenMax.to(overlaySlider, .5, {opacity: '0', ease: Sine.easeOut});

			//Animar textos contenido
			TweenMax.to(overlayTitle, .3, {opacity:'0', ease: Power3.easeOut, delay: .2});
			TweenMax.to(overlayDescription, .2, {opacity:'0', ease: Power3.easeOut, delay: .4});

			//Animar scroll content
			TweenMax.to($(".scroll-content"), .3, {opacity:'0', ease: Power3.easeOut, onComplete:hideShadowScrollDesc, delay:.3});
			TweenMax.to($("#my-scrollbar .scrollbar-track"), .3, {opacity:'0', ease: Power3.easeOut, delay:.4});

			//Animar overlay
			TweenMax.to(overlayElmt, .5, {opacity: '0', onComplete:completeCloseHandler, ease: Sine.easeIn, delay:.6});

		}else if (section== "video"){

			//Ocultar video
			TweenMax.to($(".video-container"), .5, {opacity:'0', ease: Power3.easeOut});

			//Ocultar videoOverlay
			TweenMax.to(videoOverlayElmt, .5, {backgroundColor:'rgba(0,0,0,0)', display:'none', ease: Power3.easeOut, delay:.4});

			//Stop video
			videoElmt.pause();
			videoElmt.currentTime = 0;
		}


		//Sonido cierre (sea video o slider)
		audio.play();

	}



	/**
	* @function showCloseBtn
	* @description closeBtn se muestra en 2 overlay. Overlay gral y overlay-video.
	*/
	var showCloseBtn = function(){
		TweenMax.to(containerCloseBtn, 1.8, {opacity:1, ease: Power3.easeOut});
	}


	/**
	* @function searchSliderImgs
	* @description Buscar todas las imagenes de un carpeta (para slickSlider)
	*
	* @param section  - Numero de seccion correspndiente a cada boton de pantalla principal, donde se encuentran las imgs para slider
	*
	* @important El slider hace un refresh cuando terminar de abrir por completo el overlay (completeOpenHandler). Al estar oculto, js no calcula los tamanios y no se muestra el slider.
	*/

	var searchSliderImgs = function(section){

		//Inicializar slider
//		initSlick();


		var imgsFolder = "images/"+section+"/";

		$.ajax({
			url:imgsFolder,
			success:function(data){



				//Parsear las imagenes de la carpeta y agregarlos al slider
				$(data).find("a").attr("href", function (i, val) {
            if( val.match(/\.(jpe?g|png|gif)$/) ) {
                // $(".slider").append( "<img src='"+ imgsFolder + val +"'/>" );
                $(".slider").slick("slickAdd", "<img src='"+ imgsFolder + val +"'/>" );
            }
        });



			}
		});

	}

	/**
	* @function removeSliderImgs
	* @description borra todas las imagenes dentro del slider
	*
	*/
	var removeSliderImgs = function(){

		$(".single-img-slider img").each(function(){
				console.log($(this).attr('src'));
				$(this).remove();
		});

	}

	/**
	* @function jsonData
	* @description Busca datos json
	*
	*/
	var jsonData = function(btnId){

		$.ajax({
			//url:'index.php',
			data:'getDataApp=true&btnId='+btnId,
			type:'post',
			dataType:'text',
			success:function(response){
				console.log(response);

				var parsedData = JSON.parse(response);

					console.log(parsedData);
					console.log(" ***************************** ");

					//Datos de seccion
					console.log("title: "+parsedData.title);
					overlayTitle.text(parsedData.title);
					console.log("description: "+parsedData.description);
					overlayDescription.html(parsedData.description);



					console.log(" ***************************** ");

					//Imgs de slider
					$.each(parsedData.imgs, function(i, v){
						console.log(v);
						$(".slider").slick("slickAdd", "<img src='"+ v +"'/>" );
					});


			},
			timeout:4000,
			error:function(){
				alert("Error de conexion. Error al buscar datos. Intentelo mas tardes");
			}
		});

	}


	//**
	// Slick SLider
	//**

	var initSlick = function(){

		$('.single-img-slider').slick({
			lazyLoad: 'ondemand',
			dots: true,
			infinite: true,
			arrows:true,
  	 	prevArrow:'<button type="button" class="slick-prev"></button>',
  		nextArrow:'<button type="button" class="slick-next"></button>',
			speed: 500,
			fade:true,
			cssEase:'linear',
			adaptativeHeight: true
	 });
	}


	//**
	// Scrollbar
	//**
	var iniciarScoll = function(){
		var scrollbar = Scrollbar.init(scrollElemt,
		{
			speed: 0.7,
			damping:0.08,
			alwaysShowTracks:true
		});
	}





	return {

		init: function() {

			//jsonData();
			iniciarScoll();
			initSlick();
			initEnterAnimate();
			addListeners();



		}

	}

}();


jQuery(document).ready(function() { Project.init(); });
