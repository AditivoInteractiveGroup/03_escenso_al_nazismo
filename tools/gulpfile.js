//gulp modules, packages, etc...

var gulp = require('gulp');

var sass = require('gulp-sass');

var header = require('gulp-header');

var cleanCSS = require('gulp-clean-css');

var concat = require('gulp-concat');

var rename = require("gulp-rename");

var uglify = require('gulp-uglify');

var autoprefixer = require('gulp-autoprefixer');

var pkg = require('./package.json');

var browserSync = require('browser-sync').create();

var reload = browserSync.reload;

var url = 'http://03_escenso_al_nazismo.loc/';

var banner = ['/*!\n',
' * <%= pkg.title %> v<%= pkg.version %> (<%= pkg.homepage %>)\n',
' * Copyright 2017-' + (new Date()).getFullYear(), ' <%= pkg.author %>\n',
' */\n',
'\n'
].join('');


/**
 * gulp.task => vendor
 * copy all the files from the nodes_modules to the vendor directory...
 */

 gulp.task('vendor', function() {

 	/*bootstrap*/

 	gulp.src(['./node_modules/bootstrap/dist/**/*', '!./node_modules/bootstrap/dist/css/bootstrap-grid*',

 		'!./node_modules/bootstrap/dist/css/bootstrap-reboot*']).pipe(gulp.dest('../dist/private/vendor/bootstrap'))

 	/*font awesome*/

 	gulp.src([ './node_modules/@fortawesome/**/*']).pipe(gulp.dest('../dist/private/vendor'))

 	/*jquery*/

 	gulp.src(['./node_modules/jquery/dist/*', '!./node_modules/jquery/dist/core.js'])

 	.pipe(gulp.dest('../dist/private/vendor/jquery'))

 	/*easing*/

 	gulp.src(['./node_modules/jquery.easing/*.js']).pipe(gulp.dest('../dist/private/vendor/jquery-easing'))

 	/*magnific popup*/

 	gulp.src(['./node_modules/magnific-popup/dist/*']).pipe(gulp.dest('../dist/private/vendor/magnific-popup'))

 	/*scrollreveal*/

 	gulp.src(['./node_modules/scrollreveal/dist/*.js']).pipe(gulp.dest('../dist/private/vendor/scrollreveal'))

 	/*countdown*/

 	gulp.src(['./node_modules/countdown/*.js']).pipe(gulp.dest('../dist/private/vendor/countdown'))

 	/*gsap*/

 	gulp.src(['./node_modules/gsap/**/*']).pipe(gulp.dest('../dist/private/vendor/gsap'))

 	/*slick*/

 	gulp.src(['./node_modules/slick-carousel/slick/**/*']).pipe(gulp.dest('../dist/private/vendor/slick'))

 	/*isotope*/

 	gulp.src(['./node_modules/isotope-layout/dist/*']).pipe(gulp.dest('../dist/private/vendor/isotope-layout'))

 	/*jquery validation*/

 	gulp.src(['./node_modules/jquery-validation/dist/*']).pipe(gulp.dest('../dist/private/vendor/jquery-validation'))

 	/*swal*/

 	gulp.src(['./node_modules/sweetalert2/dist/*']).pipe(gulp.dest('../dist/private/vendor/sweetalert2'))

 	/*block ui */

 	gulp.src(['./node_modules/block-ui/*']).pipe(gulp.dest('../dist/private/vendor/block-ui'))

 	/*form*/

 	gulp.src(['./node_modules/jquery-form/dist/*']).pipe(gulp.dest('../dist/private/vendor/jquery-form'))

 	/*toastr*/

 	gulp.src(['./node_modules/toastr/build/*']).pipe(gulp.dest('../dist/private/vendor/toastr'))

 	/*blue imp*/

 	gulp.src(['./node_modules/blueimp-gallery/**/*']).pipe(gulp.dest('../dist/private/vendor/blueimp-gallery'))

 	/*calendar*/

 	gulp.src(['./node_modules/add-to-calendar-buttons/*']).pipe(gulp.dest('../dist/private/vendor/add-to-calendar-buttons'))

 	/*calendar 2*/

 	gulp.src(['./node_modules/addtocalendar/*']).pipe(gulp.dest('../dist/private/vendor/addtocalendar'))

  /*Smooth scrollbar */

 	gulp.src(['./node_modules/smooth-scrollbar/**/*']).pipe(gulp.dest('../dist/private/vendor/smooth-scrollbar'))
  
  /*video.js */

  gulp.src(['./node_modules/video.js/*']).pipe(gulp.dest('../dist/private/vendor/videojs'))

 });

/**
 * gulp.task => css:compile
 * compile all the scss and autoprefeix to last versions.
 */

 gulp.task('css:compile', function() {

 	return gulp.src('../src/scss/**/*.scss')

 	.pipe(sass.sync({ outputStyle: 'expanded'}).on('error', sass.logError))

 	.pipe(autoprefixer({ browsers: ['last 2 versions'], cascade: false }))

 	.pipe(header(banner, { pkg: pkg }))

 	.pipe(gulp.dest('../dist/private/css'))

 });

/**
 * gulp.task => css:minify
 * minify and rename for min.
 */

 gulp.task('css:minify', ['css:compile'], function() {

 	return gulp.src(['../dist/private/css/*.css', '!../dist/private/css/*.min.css']).pipe(cleanCSS())

 	.pipe(rename({ suffix: '.min'}))

 	.pipe(gulp.dest('../dist/private/css'))

 	.pipe(browserSync.stream());

 });

/**
 * gulp.task => js:minify
 * minify the js distribution files.
 */

 gulp.task('js:minify', function() {

 	return gulp.src(['../src/js/*.js', '.!./src/js/*.min.js']).pipe(uglify())

 	.pipe(rename({ suffix: '.min'})).pipe(header(banner, { pkg: pkg }))

 	.pipe(gulp.dest('../dist/private/js'))

 	.pipe(browserSync.stream());

 });

/**
 * gulp.task => js:browserSync
 * Sync the browser.
 */

 gulp.task('browserSync', function() {

 	browserSync.init({notify: false,

 		host: '03_escenso_al_nazismo.loc',

 		open: 'external', injectChanges: true, proxy: url}); });

/**
 * gulp default tasks...
 * and watch...
 */

 gulp.task('css', ['css:compile', 'css:minify']);

 gulp.task('js', ['js:minify']);

 gulp.task('default', ['css', 'js', 'vendor']);

 gulp.task('watch', ['css', 'js', 'browserSync'], function() {

 	gulp.watch('../src/scss/*.scss', ['css', reload]);

 	gulp.watch('../src/js/*.js', ['js', reload]);

 	gulp.watch('../dist/*.php', browserSync.reload);

 });
